﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    class Function
    {
        public float a;
        public float b;
        public float c;
        public float delta;


        /// <summary>
        //
        /// </summary>
 //Viết hàm giải và biện luận phương trình bậc 2: ax^2 + bx + c = 0.
        public void inPut()
        {
            Console.WriteLine("Nhap vao a:");
            a = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao b: ");
            b = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao c:");
            c = Convert.ToSingle(Console.ReadLine());
        }
        public void outPut()
        {
            float x;
            float x2;
            float x1;
            x1 = x2 = x = 0;
            delta = (b * b) - (4 * a * c);
            if (a == 0)
            {
                if (b == 0 && c != 0)
                    Console.WriteLine("Phuong trinh vo nghiem");
                else
                {
                    x = -c / b;
                    Console.WriteLine("Phuong trinh co nghiem " + x.ToString());
                }
            }
            else
            {
                if (delta < 0)
                    Console.WriteLine("Phuong trinh vo nghiem!");
                else if (delta == 0)
                {
                    x = -b / 2 * a;
                    Console.WriteLine("Phuong trinh co nghiem: " + x.ToString());
                }
                else
                {
                    x1 = (-b - (float)Math.Sqrt(delta)) / 2 * a;
                    x2 = (-b + (float)Math.Sqrt(delta)) / 2 * a;
                   
                        Console.WriteLine("\nPhuong trinh {0}x^2+{1}x{2}=0 co 2 nghiem phan biet ! \n", a, b, c);
                        Console.WriteLine("Nghiem thu nhat: " + x1.ToString());
                        Console.WriteLine("Nghiem thu hai: " + x2.ToString());
                    
                    
                    
                    
                }
            }

        }
    }

    class Program2
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bai 3: Giai phuong trinh bac 2\n\n");

            Function n = new Function();
            
            n.inPut();
            n.outPut();
            Console.Read();
        }
    }
}