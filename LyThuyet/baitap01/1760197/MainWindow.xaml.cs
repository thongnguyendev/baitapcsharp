﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _1760197
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string[] arr = { 
            "\nkhông quan trọng bạn đi chậm như thế nào miễn là bạn không dừng lại.- Nho giáo",
            "\nMọi thứ bạn từng muốn là ở phía bên kia của nỗi sợ hãi. - George Addair",
            "\nChỉ có một điều khiến giấc mơ không thể đạt được: nỗi sợ thất bại. - Paulo Coelho",
            "\nCách tốt nhất để có được sự tự tin là làm những gì bạn sợ làm. - Shati Sharma",
            "\nKhông bao giờ là quá muộn để trở thành những gì bạn có thể có được. - George Eliot",
            "\nBạn không thể đủ khả năng để sống trong tiềm năng cho đến hết đời; tại một số điểm, bạn phải giải phóng tiềm năng và thực hiện di chuyển của bạn. - Eric Thomas",
            "\nTôi chưa bao giờ mơ về thành công. Tôi đã làm việc cho nó. - Estee Lauder",
            "\nThời gian của bạn là có hạn, vì vậy đừng lãng phí nó khi sống cuộc sống của người khác. - Steve Jobs",
            "\nĐể thực hiện một hành động tích cực, chúng ta phải phát triển ở đây một tầm nhìn tích cực. - Đạt Lai Lạt Ma",
            "\nHai mươi năm nữa, bạn sẽ thất vọng hơn vì những việc bạn không làm hơn là những việc bạn đã làm. - Đánh dấu",
            "\n Chỉ những người dám thất bại lớn mới có thể đạt được rất nhiều. - Robert F. Kennedy" };
            Random r = new Random();
            int dem = r.Next(0, 10);
            MessageBox.Show(arr[dem], "Chủ đề: Các câu nói hay giúp tạo động lực sống!");    
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            string[] arr = { 
            "\nHít đất 10 cái.",
            "\nChống đẩy 20 cái",
            "\nNâng tạ tay gấp (Bicep curl)",
            "\nNâng tạ tay duỗi (Lateral raise)",
            "\nNằm ngửa nâng tạ (Dumbbell floor press)",
            "\nNâng tạ squat (Dumbbell squat)",
            "\nGiữ tay, đạp chân (Deadbug)",
            "\nNhảy dây",
            "\nCăng giản lưng",
            "\nNâng gối với thân bên",
            "\nTạo tư thế  cây nến" };
            Random r = new Random();
            int dem = r.Next(0, 10);
            MessageBox.Show(arr[dem], "Chủ đề: Các động tác tập thể dục!");
        }


        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            string[] arr = { 
            "\nSúp lơ: cauliflower",
            "\nĐậu Hà Lan: peas",
            "\nKhoai mì: cassava root",
            "\nCải dầu: colza",
            "\nCải dầu: colza",
            "\nVải: lychee",
            "\nRau má: centella",
            "\nMướp: see qua hoặc loofah",
            "\nLá lốt: wild betel leaves",
            "\nMãng cầu xiêm: soursop",
            "\nĐậu xanh: mung bean" };
            Random r = new Random();
            int dem = r.Next(0, 10);
            MessageBox.Show(arr[dem], "Các cặp từ ANH-VIỆT ,Chủ đề: rau,củ quả");
        }

        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            
            MessageBox.Show( "Hello Vân xinh đẹp ! Thông developer");
        }    

    }
}
