﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace BT3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        BindingList<Array> list = null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            list = new BindingList<Array>()
            {
                new Array() { ID = "1", Fullname="\nSúp lơ: cauliflower.", Avatar="/Images/hinh1.jpg"},
                new Array() { ID = "2", Fullname="\n Peas: /piːz/  đậu hà lan", Avatar="/Images/hinh2.jpg"},
                new Array() { ID = "3", Fullname="\nKhoai mì: cassava root",  Avatar="/Images/hinh3.jpg"},
                new Array() { ID = "4", Fullname="\nCải dầu: colza", Avatar="/Images/hinh4.jpg"},
                new Array() { ID = "5", Fullname="\nVải: lychee",  Avatar="/Images/hinh5.jpg"},
                new Array() { ID = "6", Fullname="\nRau má: centella", Avatar="/Images/hinh6.jpg"},
                new Array() { ID = "7", Fullname="\nMướp: see qua hoặc loofah",  Avatar="/Images/hinh7.jpg"} ,
                new Array() { ID = "8", Fullname="\nLá lốt: wild betel leaves",  Avatar="/Images/hinh8.jpg"},
                new Array() { ID = "9", Fullname="\nMãng cầu xiêm: soursop",  Avatar="/Images/hinh9.jpg"},
                new Array() { ID = "10", Fullname="\nĐậu xanh: mung bean",  Avatar="/Images/hinh10.jpg"},
                new Array() { ID = "11", Fullname="\nDưa hấu: watermelon",  Avatar="/Images/hinh11.jpg"},
                new Array() { ID = "12", Fullname="\nCarrot:  /ˈkærət/ cà rốt", Avatar="/Images/hinh12.jpg"},
                new Array() { ID = "13", Fullname="\nTomato: /təˈmeɪtoʊ/ cà chua",  Avatar="/Images/hinh13.jpg"},
                new Array() { ID = "14", Fullname="\n Chick peas: /tʃɪk piːz / đậu hồi",  Avatar="/Images/hinh14.jpg"},
                new Array() { ID = "15", Fullname="\nCabbage:  /ˈkæbɪdʒ/ bắp cải",  Avatar="/Images/hinh15.jpg"},
                new Array() { ID = "16", Fullname="\n Beetroot: /ˈbiːtruːt/ củ cải đường",  Avatar="/Images/hinh16.jpg"},
                new Array() { ID = "17", Fullname="\nMarrow:  /ˈmæroʊ/ bí xanh",  Avatar="/Images/hinh17.jpg"},
                new Array() { ID = "18", Fullname="\nAvocado. /ˌævəˈkɑːdoʊ/ trái bơ",  Avatar="/Images/hinh18.jpg"},
                new Array() { ID = "19", Fullname="\nPapaya. /pəˈpaɪə/ trái đu đủ",  Avatar="/Images/hinh19.jpg"},
                new Array() { ID = "20", Fullname="\nPomelo. /ˈpɑːməloʊ/ trái bưởi.",  Avatar="/Images/hinh20.jpg"}

            };

            itemsControl.ItemsSource = list;

            var array = new Array()
            {
                ID = "4",
                Fullname = "Tran Duc Thang",
                Credits = 50
            };
            Change(array);
            Debug.WriteLine(array.Fullname);
        }

        void Change(Array info)
        {
            info.Fullname = "Nguyen Ai Linh";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var student = (sender as Button).DataContext as Array;
            var screen = new editarrayWindow(student);

            if (screen.ShowDialog() == true)
            {
                Debug.WriteLine("True");
            }
            else
            {
                Debug.WriteLine("False");
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem1_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }

        private void MenuItem2_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }

        private void MenuItem3_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }
    }
}

