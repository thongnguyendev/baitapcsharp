﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BT2
{

    public partial class editarrayWindow : Window
    {
        public Array c = null;

        public editarrayWindow(Array array)
        {

            InitializeComponent();
           c = array;

            avatarImage.Source = new BitmapImage(new Uri(c.Avatar, UriKind.Relative));
            fullnameTextBox.Text =c.Fullname;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            c.Fullname = fullnameTextBox.Text;


            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
