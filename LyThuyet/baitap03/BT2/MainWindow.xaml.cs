﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace BT2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        BindingList<Array> list = null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            list = new BindingList<Array>()
            {
                new Array() { ID = "1", Fullname="\n\nHít đất 10 cái.", Avatar="/Images/hinh1.jpg"},
                new Array() { ID = "2", Fullname="\nChống đẩy 20 cái", Avatar="/Images/hinh2.jpg"},
                new Array() { ID = "3", Fullname="\nNâng tạ tay gấp (Bicep curl)",  Avatar="/Images/hinh3.jpg"},
                new Array() { ID = "4", Fullname="\nNâng tạ tay duỗi (Lateral raise)", Avatar="/Images/hinh4.jpg"},
                new Array() { ID = "5", Fullname="\nNằm ngửa nâng tạ (Dumbbell floor press)",  Avatar="/Images/hinh5.jpg"},
                new Array() { ID = "6", Fullname="\nNâng tạ squat (Dumbbell squat)", Avatar="/Images/hinh6.jpg"},
                new Array() { ID = "7", Fullname="\nGiữ tay, đạp chân (Deadbug)",  Avatar="/Images/hinh7.jpg"} ,
                new Array() { ID = "8", Fullname="\nNhảy dây",  Avatar="/Images/hinh8.jpg"},
                new Array() { ID = "9", Fullname="\nCăng giản lưng",  Avatar="/Images/hinh9.jpg"},
                new Array() { ID = "10", Fullname="\nNâng gối với thân bên",  Avatar="/Images/hinh10.jpg"},
                new Array() { ID = "11", Fullname="\nTạo tư thế  cây nến",  Avatar="/Images/hinh11.jpg"},
                new Array() { ID = "12", Fullname="\nBài tập thể dục: cuộn người", Avatar="/Images/hinh12.jpg"},
                new Array() { ID = "13", Fullname="\nĐộng tác: Plank Twist",  Avatar="/Images/hinh13.jpg"},
                new Array() { ID = "14", Fullname="\nBài tập chạy bộ tại chỗ",  Avatar="/Images/hinh14.jpg"},
                new Array() { ID = "15", Fullname="\nBài tập đứng vặn mình",  Avatar="/Images/hinh15.jpg"},
                new Array() { ID = "16", Fullname="\nBài tập Aerobic bật nhảy",  Avatar="/Images/hinh16.jpg"},
                new Array() { ID = "17", Fullname="\nBài tập Aerobic lắc vòng giảm cân",  Avatar="/Images/hinh17.jpg"},
                new Array() { ID = "18", Fullname="\nBài tập step jack",  Avatar="/Images/hinh18.jpg"},
                new Array() { ID = "19", Fullname="\nBài tập giảm mỡ bụng Flutter Kicks – Đá chân",  Avatar="/Images/hinh19.jpg"},
                new Array() { ID = "20", Fullname="\nBài tập giảm mỡ bụng Oblique Crunch Left – Gập hông chéo",  Avatar="/Images/hinh20.jpg"}

            };

            itemsControl.ItemsSource = list;

            var array = new Array()
            {
                ID = "4",
                Fullname = "Tran Duc Thang",
                Credits = 50
            };
            Change(array);
            Debug.WriteLine(array.Fullname);
        }

        void Change(Array info)
        {
            info.Fullname = "Nguyen Ai Linh";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var student = (sender as Button).DataContext as Array;
            var screen = new editarrayWindow(student);

            if (screen.ShowDialog() == true)
            {
                Debug.WriteLine("True");
            }
            else
            {
                Debug.WriteLine("False");
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem1_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }

        private void MenuItem2_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }

        private void MenuItem3_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }
    }
}


