﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        BindingList<Student> list = null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            list = new BindingList<Student>()
            {
                new Student() { ID = "1", Fullname="\nMọi thứ bạn từng muốn là ở phía bên kia của nỗi sợ hãi. - George Addair-", Avatar="/Images/hinh1.jpg"},
                new Student() { ID = "2", Fullname="\nChỉ có một điều khiến giấc mơ không thể đạt được: nỗi sợ thất bại. - Paulo Coelho", Avatar="/Images/hinh2.jpg"},
                new Student() { ID = "3", Fullname="\nCách tốt nhất để có được sự tự tin là làm những gì bạn sợ làm. \n-- Shati Sharma--",  Avatar="/Images/hinh3.jpg"},
                new Student() { ID = "4", Fullname="\nkhông quan trọng bạn đi chậm như thế nào miễn là bạn không dừng lại.- Nho giáo-", Avatar="/Images/hinh4.jpg"},
                new Student() { ID = "5", Fullname="\nKhông bao giờ là quá muộn để trở thành những gì bạn có thể có được. - George Eliot",  Avatar="/Images/hinh5.jpg"},
                new Student() { ID = "6", Fullname="\nBạn không thể đủ khả năng để sống trong tiềm năng cho đến hết đời;\n Tại một số điểm, bạn phải giải phóng tiềm năng và thực hiện di chuyển của bạn. \n- Eric Thomas-", Avatar="/Images/hinh6.jpg"},
                new Student() { ID = "7", Fullname="\nTôi chưa bao giờ mơ về thành công. Tôi đã làm việc cho nó. - Estee Lauder-",  Avatar="/Images/hinh7.jpg"} ,
                new Student() { ID = "8", Fullname="\nThời gian của bạn là có hạn, vì vậy đừng lãng phí nó khi sống cuộc sống của người khác. -- Steve Jobs--",  Avatar="/Images/hinh8.jpg"},
                new Student() { ID = "9", Fullname="\nĐể thực hiện một hành động tích cực, chúng ta phải phát triển ở đây một tầm nhìn tích cực.\n - Đạt Lai Lạt Ma-",  Avatar="/Images/hinh9.jpg"},
                new Student() { ID = "10", Fullname="\nHai mươi năm nữa, bạn sẽ thất vọng hơn vì những việc bạn không làm hơn là những việc bạn đã làm.\n - Đánh dấu-",  Avatar="/Images/hinh10.jpg"},
                new Student() { ID = "11", Fullname="\nChỉ những người dám thất bại lớn mới có thể đạt được rất nhiều. -- Robert F. Kennedy--",  Avatar="/Images/hinh11.jpg"},
                new Student() { ID = "12", Fullname="\nDù bạn là ai, hãy là một người tốt. - Abraham Lincoln", Avatar="/Images/hinh12.jpg"},
                new Student() { ID = "13", Fullname="\nTự tin không phải luôn luôn đúng mà còn không sợ sai. -- Peter T. Mcintyre--",  Avatar="/Images/hinh13.jpg"},
                new Student() { ID = "14", Fullname="\nTrưởng thành đến khi bạn ngừng đưa ra lời bào chữa và bắt đầu thay đổi.",  Avatar="/Images/hinh14.jpg"},
                new Student() { ID = "15", Fullname="\nChúng ta có thể gặp nhiều thất bại \nNhưng chúng ta không được đánh bại. -- Maya Angelou--",  Avatar="/Images/hinh15.jpg"},
                new Student() { ID = "16", Fullname="\nHãy nhớ rằng không đạt được những gì bạn muốn đôi khi là một sự may mắn tuyệt vời.\n-- LamaDalai Lama--",  Avatar="/Images/hinh16.jpg"},
                new Student() { ID = "17", Fullname="\nThất bại sẽ không bao giờ vượt qua tôi nếu quyết tâm thành công của tôi đủ mạnh. -- Mand Mandino--",  Avatar="/Images/hinh17.jpg"},
                new Student() { ID = "18", Fullname="\nNếu bạn không thích cái gì đó, hãy thay đổi nó.\n Nếu bạn không thể thay đổi nó, hãy thay đổi thái độ của bạn. \n-- Maya Angelou--",  Avatar="/Images/hinh18.jpg"},
                new Student() { ID = "19", Fullname="\nĐừng ước nó dễ dàng hơn. Chúc bạn khỏe hơn. -- Jim Rohn--",  Avatar="/Images/hinh19.jpg"},
                new Student() { ID = "20", Fullname="\nTin rằng bạn có thể và bạn đang ở giữa chừng. -- Theodore Roosevelt--",  Avatar="/Images/hinh20.jpg"}

            };

            itemsControl.ItemsSource = list;

            var student = new Student()
            {
                ID = "4",
                Fullname = "Tran Duc Thang",
                Credits = 50
            };
            Change(student);
            Debug.WriteLine(student.Fullname);
        }

        void Change(Student info)
        {
            info.Fullname = "Nguyen Ai Linh";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var student = (sender as Button).DataContext as Student;
            var screen = new editstudentWindow(student);

            if (screen.ShowDialog() == true)
            {
                Debug.WriteLine("True");
            }
            else
            {
                Debug.WriteLine("False");
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem1_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }

        private void MenuItem2_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }

        private void MenuItem3_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(sender.ToString());
        }
    }
}
