﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    
    public partial class editstudentWindow : Window
    {
        public Student editedstudent = null;

        public editstudentWindow(Student student)
        {
             
            InitializeComponent();
            editedstudent = student;

            avatarImage.Source = new BitmapImage(new Uri(editedstudent.Avatar, UriKind.Relative));
            fullnameTextBox.Text = editedstudent.Fullname;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            editedstudent.Fullname = fullnameTextBox.Text;
           

            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
