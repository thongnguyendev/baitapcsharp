﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    public class Student: INotifyPropertyChanged
    {
        private string _id;
        private string _fullname;
        private string _avatar;
        private int _credits;

        
        

      public string ID{
            get { return  _id; }
            set {SetField(ref _id, value) ;}
        }
        public string Fullname {
            get{ return _fullname;}  
            set{  SetField(ref _fullname, value); }
        }
        public string Avatar{ get{ return _avatar;}
            set { SetField(ref _avatar, value); }
        }
        public int Credits
        {
            get { return _credits; }
            set { SetField(ref _credits, value); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        
           private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
    {
        var handler = PropertyChanged;
        if (handler != null)
            handler(this, new PropertyChangedEventArgs(propertyName));
    }
        

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            NotifyPropertyChanged(propertyName);
            return true;
        }
    }
}
